# whisper-test

Uses the [status sdk](https://github.com/status-im/status-go-sdk) to test the [libp2p](https://libp2p.io/) [version](https://github.com/ethereum/go-ethereum/pull/16419) of Whisper.